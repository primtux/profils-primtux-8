// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.first_run", false);
user_pref("app.normandy.migrationsApplied", 12);
user_pref("app.normandy.user_id", "05f11865-58e2-4f52-94b5-acf58594b5b4");
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1707664552);
user_pref("app.update.lastUpdateTime.background-update-timer", 1707664552);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1707664552);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 1707664552);
user_pref("app.update.lastUpdateTime.region-update-timer", 1707664552);
user_pref("app.update.lastUpdateTime.rs-experiment-loader-timer", 1707664552);
user_pref("app.update.lastUpdateTime.services-settings-poll-changes", 1707664552);
user_pref("app.update.lastUpdateTime.telemetry_modules_ping", 1707664581);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1707664552);
user_pref("browser.bookmarks.addedImportButton", true);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.contentblocking.category", "standard");
user_pref("browser.contextual-services.contextId", "{5b44292b-2066-4993-a1c0-a804d22e5771}");
user_pref("browser.download.viewableInternally.typeWasRegistered.avif", true);
user_pref("browser.download.viewableInternally.typeWasRegistered.webp", true);
user_pref("browser.engagement.home-button.has-used", true);
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1707664521);
user_pref("browser.laterrun.bookkeeping.sessionCount", 1);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 142);
user_pref("browser.newtabpage.activity-stream.discoverystream.endpointSpocsClear", "https://spocs.mozilla.net/user");
user_pref("browser.newtabpage.activity-stream.discoverystream.endpoints", "https://getpocket.cdn.mozilla.net/,https://firefox-api-proxy.cdn.mozilla.net/,https://spocs.getpocket.com/,https://spocs.mozilla.net/");
user_pref("browser.newtabpage.activity-stream.discoverystream.rec.impressions", "{\"472652042\":1707664533882,\"2799532621393798\":1707664533851,\"1415985606995060\":1707664533885}");
user_pref("browser.newtabpage.activity-stream.discoverystream.spoc.impressions", "{\"462074015\":[1707664533860]}");
user_pref("browser.newtabpage.activity-stream.discoverystream.spocs-endpoint", "https://spocs.mozilla.net/spocs");
user_pref("browser.newtabpage.activity-stream.impressionId", "{4af06a9c-9412-4264-b9db-07cd1f6dbb8f}");
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pageActions.persistedActions", "{\"ids\":[\"bookmark\"],\"idsInUrlbar\":[\"bookmark\"],\"idsInUrlbarPreProton\":[],\"version\":1}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.policies.applied", true);
user_pref("browser.proton.toolbar.version", 3);
user_pref("browser.region.update.updated", 1707664524);
user_pref("browser.safebrowsing.provider.google4.lastupdatetime", "1707664577978");
user_pref("browser.safebrowsing.provider.google4.nextupdatetime", "1707666356978");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1707664530782");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1707686130782");
user_pref("browser.search.region", "FR");
user_pref("browser.search.widget.inNavBar", true);
user_pref("browser.shell.mostRecentDateSetAsDefault", "1707664529");
user_pref("browser.startup.couldRestoreSession.count", 1);
user_pref("browser.startup.homepage", "https://qwantjunior.fr");
user_pref("browser.startup.homepage_override.buildID", "20240205133611");
user_pref("browser.startup.homepage_override.mstone", "122.0.1");
user_pref("browser.startup.lastColdStartupCheck", 1707664523);
user_pref("browser.topsites.contile.cacheValidFor", 10384);
user_pref("browser.topsites.contile.cachedTiles", "[{\"id\":74357,\"name\":\"Amazon\",\"url\":\"https://www.amazon.fr/?tag=admarketpla00-21&ref=pd_sl_de164c24fce8a8d8717f31d95b65b8b038d1a87fa52019fbd6d5d54e&mfadid=adm\",\"click_url\":\"https://bridge.sfo1.ap01.net/ctp?version=16.0.0&key=1707661977400600001.1&ci=1707661977599.12791&ctag=pd_sl_de164c24fce8a8d8717f31d95b65b8b038d1a87fa52019fbd6d5d54e\",\"image_url\":\"https://contile-images.services.mozilla.com/obgoOYObjIFea_bXuT6L4LbBJ8j425AD87S1HMD3BWg.9991.jpg\",\"image_size\":200,\"impression_url\":\"https://imp.mt48.net/static?id=7RHzfOIXjFEYsBdvIpkX4Zbr4QHX1Cxr4pbW4QbWfpbX7ReNxR3UIG8zInwYIFIVs9eYiGINHrwkiFXwxYIZjF8XgClWfC2X7R4dHQ8zJnErj%3DcO7R4dHQfz4Z2Z4CxnHG3Z5FwqgCfX1p8d4Cxr1BINI9HuiF2z4Z2Z4CxnHmcux%3DcvImauiF2zfQlX4px%2B7ncqIr7VjGbuiF2zfCfr4CbY4Zfnj9wWIBdvIpkY7R4TJr2uJREuHnEYgClnHr7wHG3vjnDuiF2z4Z2r7ReTHF4wJFEUjtdvIpkXfpbr\"},{\"id\":74038,\"name\":\"Hotels.com\",\"url\":\"https://fr.hotels.com/?locale=fr_FR&pos=HCOM_FR&siteid=300000010&rffrid=sem.hcom.FR.AMP.003.00.03.s.s.kwrd=device.creative.15480311.210327.adposition.targetid.loc_physical_ms.loc_interest_ms.keyword.1707661977400600001.aw.ds&PSRC=_psrc&semcid=HCOM-FR.UB.ADMARKETPLACE.GT-C-FR.HOTEL&SEMDTL=a1210327.b115480311.g1targetid.l1.e1device.m1gclid.r1_expediakwid.c1_expediaadid.j1loc_physical_ms.k1loc_interest_ms.d1creative.h1matchtype.i1feeditemid.n1.o1.p1.q1.s1.t1.x1.f1.u1.v1.w1&mfadid=adm\",\"click_url\":\"https://bridge.sfo1.admarketplace.net/ctp?version=16.0.0&key=1707661977400600001.2&ci=1707661977599.12791&ctag=1707661977400600001\",\"image_url\":\"https://contile-images.services.mozilla.com/NFID2PaNJaR3mcE18BFgudhY6mkekkTZOy7CcB27QzE.8480.jpg\",\"image_size\":200,\"impression_url\":\"https://imp.mt48.net/static?id=7RHzfOIXjFEYsBdvIpkX4Zbr4QHX1Cxr4pbW4QbWfpbX7ReNxR3UIG8zInwYIFIVs9eYiGINHrwkiFXwxYIZjF8XgClWfC2X7R4dHQ8zJnErj%3DcO7R4dHQfz4Z2WfZLnHG3Z5FwqgCfX1p8d4Cxr1BINI9HuiF2z4Z2WfZLnHmcux%3DcvImauiF2zfQlWfZ8r7ncqIr7VjGbuiF2zfCDk1pbZfClnj9wWIBdvIpkY7R4TJr2uJREuHnEYgC8nHr7wHG3vjnDuiF2z4pHZfBIWJ%3DcQIFdwJR2uiF2zfCbW4Wyy\"}]");
user_pref("browser.topsites.contile.lastFetch", 1707664525);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"unified-extensions-area\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"urlbar-container\",\"zoom-controls\",\"search-container\",\"downloads-button\",\"unified-extensions-button\",\"ublock0_raymondhill_net-browser-action\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"firefox-view-button\",\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"import-button\",\"personal-bookmarks\"]},\"seen\":[\"save-to-pocket-button\",\"developer-button\",\"ublock0_raymondhill_net-browser-action\"],\"dirtyAreaCache\":[\"nav-bar\",\"PersonalToolbar\"],\"currentVersion\":20,\"newElementCount\":3}");
user_pref("browser.urlbar.placeholderName", "Google");
user_pref("browser.urlbar.quicksuggest.migrationVersion", 2);
user_pref("browser.urlbar.quicksuggest.scenario", "history");
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1707664526586");
user_pref("distribution.iniFile.exists.appversion", "122.0.1");
user_pref("distribution.iniFile.exists.value", true);
user_pref("distribution.mint-001.bookmarksProcessed", true);
user_pref("doh-rollout.doneFirstRun", true);
user_pref("doh-rollout.home-region", "FR");
user_pref("dom.forms.autocomplete.formautofill", true);
user_pref("dom.push.userAgentID", "717d0aed37664815b7117ba7301001ce");
user_pref("extensions.activeThemeID", "default-theme@mozilla.org");
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.databaseSchema", 35);
user_pref("extensions.getAddons.cache.lastUpdate", 1707664552);
user_pref("extensions.getAddons.databaseSchema", 6);
user_pref("extensions.installedDistroAddon.langpack-en-CA@firefox.mozilla.org", true);
user_pref("extensions.installedDistroAddon.langpack-en-GB@firefox.mozilla.org", true);
user_pref("extensions.installedDistroAddon.langpack-fr@firefox.mozilla.org", true);
user_pref("extensions.lastAppBuildId", "20240205133611");
user_pref("extensions.lastAppVersion", "122.0.1");
user_pref("extensions.lastPlatformVersion", "122.0.1");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.pictureinpicture.enable_picture_in_picture_overrides", true);
user_pref("extensions.quarantinedDomains.list", "autoatendimento.bb.com.br,ibpf.sicredi.com.br,ibpj.sicredi.com.br,internetbanking.caixa.gov.br,www.ib12.bradesco.com.br,www2.bancobrasil.com.br");
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.ui.dictionary.hidden", true);
user_pref("extensions.ui.extension.hidden", false);
user_pref("extensions.ui.lastCategory", "addons://list/extension");
user_pref("extensions.ui.locale.hidden", false);
user_pref("extensions.ui.sitepermission.hidden", true);
user_pref("extensions.webcompat.enable_shims", true);
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.screenshots@mozilla.org", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.uBlock0@raymondhill.net", true);
user_pref("extensions.webextensions.uuids", "{\"formautofill@mozilla.org\":\"27d6bff6-1f26-434d-853c-7b0413a61845\",\"pictureinpicture@mozilla.org\":\"77c15248-0b16-4f6b-88b7-8dfdf844842e\",\"screenshots@mozilla.org\":\"e1c42b60-79a7-46d5-90d2-59293caf769f\",\"webcompat-reporter@mozilla.org\":\"11b12f33-bec1-43d6-858b-49a015e6d8a9\",\"webcompat@mozilla.org\":\"9b4e9576-3f5e-4b26-9098-1beef9a2abda\",\"default-theme@mozilla.org\":\"2894d34d-974a-4d1b-8b61-21944591a5a0\",\"addons-search-detection@mozilla.com\":\"792a7503-1092-4f5d-af49-b743053a7b76\",\"google@search.mozilla.org\":\"b23e1ac8-7523-42a4-8fd3-942799236256\",\"wikipedia@search.mozilla.org\":\"183736dd-1da8-4729-a600-0765f875f91d\",\"bing@search.mozilla.org\":\"d1a4c6d8-0116-44c6-acc0-baf7829d4827\",\"ddg@search.mozilla.org\":\"e0085fb6-9b70-4b32-92ca-111a3d84f1b6\",\"ebay@search.mozilla.org\":\"111c3c09-7245-4356-a808-7d243722e712\",\"qwant@search.mozilla.org\":\"abbb8527-2f1a-43f8-aa61-22d75c6cff8c\",\"amazon@search.mozilla.org\":\"287a633c-79c3-4fd4-94f8-039209d1b8bb\",\"uBlock0@raymondhill.net\":\"d81a86fd-4de5-48b4-be87-b0cf85005b25\"}");
user_pref("gecko.handlerService.defaultHandlersVersion", 1);
user_pref("identity.fxaccounts.toolbar.defaultVisible", true);
user_pref("media.gmp-gmpopenh264.abi", "x86_64-gcc3");
user_pref("media.gmp-gmpopenh264.hashValue", "53a58bfb4c8124ad4f7655b99bfdea290033a085e0796b19245b33b91c0948fdac9f0c3e817130b352493a65d9a7a0fc8a7c1eedc618cdaa2b4580734a11cd9c");
user_pref("media.gmp-gmpopenh264.lastDownload", 1707664553);
user_pref("media.gmp-gmpopenh264.lastInstallStart", 1707664553);
user_pref("media.gmp-gmpopenh264.lastUpdate", 1707664553);
user_pref("media.gmp-gmpopenh264.version", "2.3.2");
user_pref("media.gmp.storage.version.observed", 1);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.predictor.enabled", false);
user_pref("network.prefetch-next", false);
user_pref("nimbus.syncdefaultsstore.fxaButtonVisibility", "{\"slug\":\"mozillaaccounts-toolbar-button-default-visibility-existing-user\",\"branch\":{\"slug\":\"treatment-a\",\"ratio\":1,\"feature\":{\"value\":null,\"enabled\":true,\"featureId\":\"fxaButtonVisibility\"},\"features\":null},\"active\":true,\"experimentType\":\"rollout\",\"source\":\"rs-loader\",\"userFacingName\":\"MozillaAccounts toolbar button default visibility rollout\",\"userFacingDescription\":\"This experiment measures the impact of showing the Firefox Accounts toolbar button by default for signed-out users.\",\"lastSeen\":\"2024-02-11T15:15:27.404Z\",\"featureIds\":[\"fxaButtonVisibility\"],\"prefs\":[{\"name\":\"identity.fxaccounts.toolbar.defaultVisible\",\"branch\":\"user\",\"featureId\":\"fxaButtonVisibility\",\"variable\":\"boolean\",\"originalValue\":null}],\"isRollout\":true}");
user_pref("nimbus.syncdefaultsstore.fxaButtonVisibility.boolean", true);
user_pref("nimbus.syncdefaultsstore.pocketNewtab", "{\"slug\":\"spocs-endpoint-rollout-release\",\"branch\":{\"slug\":\"control\",\"ratio\":1,\"feature\":{\"value\":null,\"enabled\":true,\"featureId\":\"pocketNewtab\"},\"features\":null},\"active\":true,\"experimentType\":\"rollout\",\"source\":\"rs-loader\",\"userFacingName\":\"spocs endpoint rollout (release)\",\"userFacingDescription\":\"Roll out change in sponsored content endpoint for new tab.\",\"lastSeen\":\"2024-02-11T15:15:28.371Z\",\"featureIds\":[\"pocketNewtab\"],\"prefs\":[{\"name\":\"browser.newtabpage.activity-stream.discoverystream.spocs-endpoint\",\"branch\":\"user\",\"featureId\":\"pocketNewtab\",\"variable\":\"spocsEndpoint\",\"originalValue\":null},{\"name\":\"browser.newtabpage.activity-stream.discoverystream.endpointSpocsClear\",\"branch\":\"user\",\"featureId\":\"pocketNewtab\",\"variable\":\"spocsClearEndpoint\",\"originalValue\":null},{\"name\":\"browser.newtabpage.activity-stream.discoverystream.endpoints\",\"branch\":\"user\",\"featureId\":\"pocketNewtab\",\"variable\":\"spocsEndpointAllowlist\",\"originalValue\":null}],\"isRollout\":true}");
user_pref("nimbus.syncdefaultsstore.pocketNewtab.spocsClearEndpoint", "https://spocs.mozilla.net/user");
user_pref("nimbus.syncdefaultsstore.pocketNewtab.spocsEndpoint", "https://spocs.mozilla.net/spocs");
user_pref("nimbus.syncdefaultsstore.pocketNewtab.spocsEndpointAllowlist", "https://getpocket.cdn.mozilla.net/,https://firefox-api-proxy.cdn.mozilla.net/,https://spocs.getpocket.com/,https://spocs.mozilla.net/");
user_pref("nimbus.syncdefaultsstore.upgradeDialog", "{\"slug\":\"upgrade-spotlight-rollout\",\"branch\":{\"slug\":\"treatment\",\"ratio\":1,\"feature\":{\"value\":null,\"enabled\":true,\"featureId\":\"upgradeDialog\"},\"features\":null},\"active\":true,\"experimentType\":\"rollout\",\"source\":\"rs-loader\",\"userFacingName\":\"Upgrade Spotlight Rollout\",\"userFacingDescription\":\"Experimenting on onboarding content when you upgrade Firefox.\",\"lastSeen\":\"2024-02-11T15:15:28.478Z\",\"featureIds\":[\"upgradeDialog\"],\"prefs\":[],\"isRollout\":true}");
user_pref("nimbus.syncdefaultsstore.upgradeDialog.enabled", false);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("privacy.cpd.offlineApps", true);
user_pref("privacy.cpd.siteSettings", true);
user_pref("privacy.sanitize.pending", "[]");
user_pref("security.sandbox.content.tempDirSuffix", "c96fa9db-182d-48f9-9949-8aba83c5a9db");
user_pref("services.settings.blocklists.addons-bloomfilters.last_check", 1707664552);
user_pref("services.settings.blocklists.gfx.last_check", 1707664552);
user_pref("services.settings.clock_skew_seconds", 0);
user_pref("services.settings.last_etag", "\"1707659829588\"");
user_pref("services.settings.last_update_seconds", 1707664552);
user_pref("services.settings.main.addons-manager-settings.last_check", 1707664552);
user_pref("services.settings.main.anti-tracking-url-decoration.last_check", 1707664552);
user_pref("services.settings.main.cfr.last_check", 1707664552);
user_pref("services.settings.main.cookie-banner-rules-list.last_check", 1707664552);
user_pref("services.settings.main.devtools-compatibility-browsers.last_check", 1707664552);
user_pref("services.settings.main.devtools-devices.last_check", 1707664552);
user_pref("services.settings.main.doh-config.last_check", 1707664552);
user_pref("services.settings.main.doh-providers.last_check", 1707664552);
user_pref("services.settings.main.fingerprinting-protection-overrides.last_check", 1707664552);
user_pref("services.settings.main.hijack-blocklists.last_check", 1707664552);
user_pref("services.settings.main.language-dictionaries.last_check", 1707664552);
user_pref("services.settings.main.message-groups.last_check", 1707664552);
user_pref("services.settings.main.nimbus-desktop-experiments.last_check", 1707664552);
user_pref("services.settings.main.normandy-recipes-capabilities.last_check", 1707664552);
user_pref("services.settings.main.partitioning-exempt-urls.last_check", 1707664552);
user_pref("services.settings.main.password-recipes.last_check", 1707664552);
user_pref("services.settings.main.password-rules.last_check", 1707664552);
user_pref("services.settings.main.pioneer-study-addons-v1.last_check", 1707664552);
user_pref("services.settings.main.query-stripping.last_check", 1707664552);
user_pref("services.settings.main.search-config-overrides.last_check", 1707664552);
user_pref("services.settings.main.search-config-v2.last_check", 1707664552);
user_pref("services.settings.main.search-config.last_check", 1707664552);
user_pref("services.settings.main.search-default-override-allowlist.last_check", 1707664552);
user_pref("services.settings.main.search-telemetry-v2.last_check", 1707664552);
user_pref("services.settings.main.sites-classification.last_check", 1707664552);
user_pref("services.settings.main.top-sites.last_check", 1707664552);
user_pref("services.settings.main.translations-identification-models.last_check", 1707664552);
user_pref("services.settings.main.translations-models.last_check", 1707664552);
user_pref("services.settings.main.translations-wasm.last_check", 1707664552);
user_pref("services.settings.main.url-classifier-skip-urls.last_check", 1707664552);
user_pref("services.settings.main.websites-with-shared-credential-backends.last_check", 1707664552);
user_pref("services.sync.clients.lastSync", "0");
user_pref("services.sync.declinedEngines", "");
user_pref("services.sync.engine.addresses.available", true);
user_pref("services.sync.globalScore", 0);
user_pref("services.sync.nextSync", 0);
user_pref("toolkit.startup.last_success", 1707664519);
user_pref("toolkit.telemetry.cachedClientID", "c2004177-3a9d-46c7-9053-8a91e839c3dd");
user_pref("toolkit.telemetry.pioneer-new-studies-available", true);
user_pref("toolkit.telemetry.previousBuildID", "20240205133611");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("trailhead.firstrun.didSeeAboutWelcome", true);
