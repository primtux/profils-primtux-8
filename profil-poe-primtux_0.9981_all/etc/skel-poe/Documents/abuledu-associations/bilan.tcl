############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : bilan.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: bilan.tcl,v 1.3 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#bilan.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

source path.tcl
source msg.tcl

global plateforme LogHome file
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
initlog $plateforme $ident

set c .frame.c
set bgn #ffff80
set bgl #ff80c0

. configure -background black -width 640 -height 480
wm geometry . +0+0

set file [file join $LogHome $argv]


wm title . Bilan\040[lindex [split [lindex [split $file /] end] .] 0]

menu .menu -tearoff 0
# Creation du menu Fichier
menu .menu.fichier -tearoff 0
.menu add cascade -label [mc {Fichier}] -menu .menu.fichier
.menu.fichier add command -label [mc {Effacer la fiche}] -command "effacefiche"
.menu.fichier add command -label [mc {Imprimer la fiche}] -command "imprimefiche"
.menu.fichier add command -label [mc {Fermer}] -command "exit"

. configure -menu .menu

text .text -yscrollcommand ".scroll set" -setgrid true -width 49 -height 20 -wrap word -background black -font {Arial 16}
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both

.text tag configure green -foreground green
.text tag configure red -foreground red
.text tag configure yellow -foreground yellow
.text tag configure normal -foreground black
.text tag configure white -foreground white


#global listeval
if {[catch { set f [open [file join $file] "r" ] }] } {
 
      set answer [tk_messageBox -message "Erreur de fichier" -type ok -icon info] 
      exit
}
##################################################
set g [open [file join $LogHome tmp.htm] "w" ] 
puts $g  "<html><body>"
puts $g  "<p align = center><B>Fiche de [string map {.log \040} [file tail $user]]</B></p>"

       while {![eof $f]} {
         set listeval [gets $f]
         .text insert end [lindex $listeval 0]\040[lindex $listeval 1]\n white
	   puts $g  "<p><font color=black>[lindex $listeval 0]\040[lindex $listeval 1]</font></p>"
            for {set i 1} {$i < [expr [llength $listeval] -1]} {incr i 1} { 

			switch [lindex [lindex $listeval [expr $i + 1]] 0] { 
                   1 {
			.text insert end [lindex [lindex $listeval [expr $i + 1]] 1]\n yellow
			puts $g  "<p><B><font color=yellow>[lindex [lindex $listeval [expr $i + 1]] 1]</font></B></p>" 
			}
                   2 {
			.text insert end [lindex [lindex $listeval [expr $i + 1]] 1]\n green
			puts $g  "<p><B><font color=green>[lindex [lindex $listeval [expr $i + 1]] 1]</font></B></p>" 			
			}
                   3 {
			.text insert end [lindex [lindex $listeval [expr $i + 1]] 1]\n red
			puts $g  "<p><B><font color=red>[lindex [lindex $listeval [expr $i + 1]] 1]</font></B></p>" 			
			}
                   }

	}
          }
	   .text insert end \n
            
 
close $f
puts $g  "</body></html>"
close $g



#######################################################"               
proc imprimefiche {} {
global progaide LogHome
set fichier [file join [pwd] $LogHome tmp.htm]
exec $progaide $fichier &
}

proc effacefiche {} {
global file LogHome
set response [tk_messageBox -message [mc {Voulez-vous vraiment effacer la fiche ?}] -type yesno ]
	if {$response == "yes"} {
	set f [open [file join $file] "w" ]
	close $f
	.text delete 0.0 end
	set g [open [file join $LogHome tmp.htm] "w" ]
	close $g 
}
}









